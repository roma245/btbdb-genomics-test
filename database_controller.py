#!/usr/bin/env python
# -*- coding: utf-8 -*- 
# Code connects to the SQLite databse and generates a folder with a name of the SRA identifier containing email address of the user and SRA identifier in separate *.txt files
# Matvey Sprindzuk, MD, software engineer, +375 33 682 57 55, bioinformatics_bel@yahoo.com

import sqlite3 as lite # import a database connector
import sys
import os
import shutil
import subprocess # built-in module for calling external software
from executor import execute # subprocess module wrapper, run "sudo pip install executor" if not have it yet
from mega import Mega # sudo pip install mega.py (package for uploading files to MegaNZ service)

con = lite.connect('db.sqlite3') # connect local database in the current directory
cur = con.cursor()  # create a cursor 
 
cur.execute("SELECT* FROM tb_genomedataprocessingrequest WHERE id = 5") # run the SQL statement on a predefined database table, change "id" as you actually need, then run script

data = cur.fetchone() # assign a variable to the executed SQL statement

print (data) # print out the results

cur.close()
con.close() # close the database connection

with open('output_csv.csv', 'w') as f: # save the results to a CSV files on a local drive in the current directory
        f.write(str(data))
        
f = open('output_csv.csv', 'w') # open the saved data

f.close()

print (data[1]) # print out the email adress separately
print (data[3]) # print out the SRA identifier separately

with open('sra_id.txt', 'w') as g: # saving SRA identifier separately in a TXT file
        g.write(str(data[3]))
g.close()

with open('email_adress.txt', 'w') as h: # saving email adress separately in a TXT file
        h.write(str(data[1]))        
h.close()

newpath = r'temporary_folder' # create a temporary folder to save the data files from the genomics data processing request and then rename it to the SRA identifier name
if not os.path.exists(newpath):
    os.makedirs(newpath)
   # os.rename("temporary_folder",(data[3]))

files = ['sra_id.txt', 'email_adress.txt'] 
for i in files:
    shutil.copy(i, 'Tuberculosis_genomics_data_processor_software')   # copy txt files into the already created directory, where data processing software codes reside
    


files = ['sra_id.txt', 'email_adress.txt'] # move saved to disk files into the temporary folder
for f in files:
    shutil.copy(f, 'ENGINE')
    
files = ['sra_id.txt', 'output_csv.csv', 'email_adress.txt'] # move saved to disk files into the temporary folder
for f in files:
    shutil.copy(f, 'temporary_folder')



execute('bash ./ENGINE/MAIN_DATA_ENGINE.sh') # calling and running a Linux Shell genomics data processing codes from a *.sh script
execute('bash ./ENGINE/ASSEMBLY.sh') # calling and running a Linux Shell genomics data processing codes from a *.sh script
execute('bash ./ENGINE/ANNOTATION.sh') # calling and running a Linux Shell genomics data processing codes from a *.sh script
execute('bash ./ENGINE/MOVE_FILES_REMOVE_REDUNDANT.sh')
execute('bash ./ENGINE/TARGET_MUTATIONS.sh')

#mega = Mega({'verbose':True}) # codes for uploading a zip archived files to the free Mega cloud storage
#m = mega.login('XXXXXXXX', 'XXXXXXXXXXX') # put your account data here
## m = mega.login()
#space = m.get_storage_space(kilo=True)
#file = m.upload('processedfiles.zip')
#m.get_upload_link(file)
## m.get_link(file)
 
os.rename("temporary_folder",(data[3])) # renaming temporary folder to the current SRA identifier name


######################################################################## END OF THE WORKING CODES #####################################################################

# con_two = lite.connect('db.sqlite3') # connect local database in a current directory # connecting database again if you want to delete a processed order and its relevant data

# cur_d = con_two.cursor()  # create a cursor 

# cur_d.execute("DELETE FROM tb_genomedataprocessingrequest WHERE id = 2")  

# # cur_d.execute("DELETE FROM tb_genomedataprocessingrequest WHERE id = (SELECT MAX(id) FROM tb_genomedataprocessingrequest)") # delete an already processed record from the database and run the software code on a new record
# result = cur_d.fetchone() # assign a variable to the executed SQL statement


# cur_d.close() # close the cursor  

# con_two.close() # close the database connection
