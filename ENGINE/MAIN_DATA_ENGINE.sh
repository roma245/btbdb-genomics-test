#!/bin/bash
# Matvey Sprindzuk, MD, computer scientist,  bioinformatics_bel@yahoo.com. +375336825755, UIIP, Minsk, Belarus
# These script is the heart of software

# first step is fetching sequence

list=`cat sra_id.txt` # read the SRA file IDs.
for i in $list
do
	echo $i

	./fastq-dump --split-files $i -v

done


# mv 0001.fastq ./ENGINE
# mv 0002.fastq ./ENGINE


# renaming
a=1
for i in *.fastq; do
  new=$(printf "%04d.fastq" "$a") #04 pad to length of 4
  mv -- "$i" "$new"
  let a=a+1
done




seqtk trimfq 0001.fastq > trimmed_fastq_1.fastq # trimming FASTQ files for better quality
seqtk trimfq 0002.fastq > trimmed_fastq_2.fastq


mv trimmed_fastq_1.fastq ./ENGINE
mv trimmed_fastq_2.fastq ./ENGINE

cd ./ENGINE # change the directory accordingly

bwa index ref.fasta # indexing reference genome file

bwa mem ref.fasta trimmed_fastq_1.fastq trimmed_fastq_2.fastq > aligned_sam_result.sam # doing alignment with BWA engine

# third step convertation

# data processing step is based on the tutorial http://www.htslib.org/workflow/


samtools view  -bS aligned_sam_result.sam > unsorted_bam.bam # coverting to bam

samtools sort unsorted_bam.bam file.sorted

samtools index file.sorted.bam # indexing


java -Xmx16G -jar pilon-1.21.jar --genome  ref.fasta --frags file.sorted.bam  --output pilon_output.pilon  --vcf 

##--vcfqe --outdir directory ./ # you can add other parameters
 
## fifh step is reducing VCF
 
## Reducing the size and filtering content of the Pilon VCF http://www.abeel.be/wiki/Variant_Calling

java -jar reducevcf.jar -i pilon_output.pilon.vcf -o reducedPilonOutputFileFinal.vcf

# sixth step  is decomposition of the VCF from the previous step

sed 's/AL123456.3/AL123456/' reducedPilonOutputFileFinal.vcf > reducedPilonOutputFileFinalBetter.vcf # remove special characters from chromosome name (https://www.gnu.org/software/sed/manual/sed.html, https://www.ncbi.nlm.nih.gov/nuccore/AL123456.3)

./vt decompose -s -o DecomposedPilonReducedresult.vcf  reducedPilonOutputFileFinalBetter.vcf  # arguments in vice versa position, first is output (http://genome.sph.umich.edu/wiki/Vt)

## seventh step is creating final genome annotation and producing a list of mutated genes

java -Xmx5G -jar snpEff.jar -c snpEff.config -s PilonSNPEffOutputStats.html -v -no-downstream -no-upstream Customtuberculosis DecomposedPilonReducedresult.vcf > PilonAnnotatedSNPeffResults.vcf # running annotation on a non-normalized VCF

###################################### running additional tools on a produced VCF file ######################################################

java -jar ./RTG.jar vcfstats ./PilonAnnotatedSNPeffResults.vcf > RTG_VCF_Stats_Results.txt # run RTG software tool to get statistics on a VCF file

############################################## producing target mutation profile results ####################################################

# code for VCF 2 BED conversion and running set intesection against a custom .bed file with a predefined mutation list
# bedops convert2bed -i < reducedPilonOutputFileFinal.vcf > converted2bedvcf.bed

#vcf2bed < reducedPilonOutputFileFinalBetter.vcf  > converted2bedvcf.bed

#bedtools intersect  -a g7.bed -b converted2bedvcf.bed > TuberculosisSampleDrugResistanceReport!!! #-filenames
 
#bedtools intersect  -a g7.bed -b converted2bedvcf.bed > TuberculosisSampleDrugResistanceReport.csv


################################################# performing De Novo assembly ###############################################################

#./a5_pipeline.pl trimmed_fastq_1.fastq  trimmed_fastq_2.fastq genome_assembled_with_a5_miseq # Run De Novo assembly with A5 assembler module


################################################# running additional annotation software on the Pilon fasta results and on De Novo assembly results ###############################


#./prokka/bin/prokka pilon_output.pilon.fasta -- outdir prokkacurrentassembly_pilon --prefix prokka_assembled_2019 --force

#./prokka/bin/prokka genome_assembled_with_a5_miseq.fasta -- outdir prokkacurrentassembly_denovo_a5 --prefix prokka_a5_assembled_2019 --force
 
#####################################################  moving all processed files from the current working directory #######################################################################

#mv testfile ./processedfiles
#mv 0001.fastq ./processedfiles
#mv 0002.fastq ./processedfiles
#mv trimmed_fastq_1.fastq ./processedfiles
#mv trimmed_fastq_2.fastq./processedfiles
#mv aligned_sam_result.sam ./processedfiles
#mv unsorted_bam.bam ./processedfiles
#mv file.sorted.bam ./processedfiles
#mv pilon_output.pilon.vcf ./processedfiles
#mv reducedPilonOutputFileFinal.vcf ./processedfiles
#mv reducedPilonOutputFileFinalBetter.vcf ./processedfiles
#mv DecomposedPilonReducedresult.vcf ./processedfiles
#mv PilonSNPEffOutputStats.html ./processedfiles
#mv PilonSNPEffOutputStats.genes.txt ./processedfiles
#mv PilonAnnotatedSNPeffResults.vcf ./processedfiles
#mv reducedPilonOutputFileFinal.vcf.log ./processedfiles
#mv pilon_output.pilon.fasta ./processedfiles
#mv sra_id.txt ./processedfiles
#mv TuberculosisSampleDrugResistanceReport!!! ./processedfiles
#mv converted2bedvcf.bed ./processedfiles
#mv TuberculosisSampleVirulenceReport!!! ./processedfiles
#mv TuberculosisSampleVirulenceReport.csv ./processedfiles
#mv TuberculosisSampleDrugResistanceReport.csv ./processedfiles
#mv RTG_VCF_Stats_Results.txt ./processedfiles
#mv prokkacurrentassembly_pilon ./processedfiles
#mv prokkacurrentassembly_denovo_a5 ./processedfiles

## the end


