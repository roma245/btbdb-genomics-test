#!/bin/bash
# These codes move the hot processed files from the ENGINE directory to the temporary folder, check the path accordinly

mv testfile /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv 0001.fastq /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv 0002.fastq /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv trimmed_fastq_1.fastq /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv trimmed_fastq_2.fastq /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv aligned_sam_result.sam home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv unsorted_bam.bam home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv file.sorted.bam /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv pilon_output.pilon.vcf /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv reducedPilonOutputFileFinal.vcf /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv reducedPilonOutputFileFinalBetter.vcf /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv DecomposedPilonReducedresult.vcf /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv PilonSNPEffOutputStats.html /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv PilonSNPEffOutputStats.genes.txt /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv PilonAnnotatedSNPeffResults.vcf /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv reducedPilonOutputFileFinal.vcf.log /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv pilon_output.pilon.fasta /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv sra_id.txt /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv TuberculosisSampleDrugResistanceReport!!! /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv converted2bedvcf.bed /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv TuberculosisSampleVirulenceReport!!! /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv TuberculosisSampleVirulenceReport.csv /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv TuberculosisSampleDrugResistanceReport.csv /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv RTG_VCF_Stats_Results.txt /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv prokkacurrentassembly_pilon /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv prokkacurrentassembly_denovo_a5 /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
################################################################################################### newly added ############
mv email_adress.txt /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv aligned_sam_result.sam /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv unsorted_bam.bam /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder

mv /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/0001.fastq /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/0002.fastq /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder

mv /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/email_adress.txt /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/output_csv.csv /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/sra_id.txt /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/Tuberculosis_genomics_data_processor_software /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder


