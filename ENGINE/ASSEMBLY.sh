#!/bin/bash

# These codes annotate the A5 assembler de novo assembled genome and a mutation profile Fasta file coming from the Pilon software
# Sprindzuk Matvey, software engineer, bioinformatics_bel@yahoo.com


./a5_pipeline.pl trimmed_fastq_1.fastq  trimmed_fastq_2.fastq genome_assembled_with_a5_miseq # performing genome assembly into the defined folder


mv genome_assembled_with_a5_miseq.s1 /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv genome_assembled_with_a5_miseq.s2 /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
mv *genome_assembled_with_a5_miseq*  /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder

#mv *.fastq  /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder # moving processed files into the one single folder
#mv *.fasta /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.gz /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.csv /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.tmplibs  /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.bai /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.qvl  /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.bam  /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.agp  /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.fsa  /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv genome_assembled_with_a5_miseq.s1 /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv genome_assembled_with_a5_miseq.s2 /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder

# the end
 
# thank you for your patience and attention


















