#!/bin/bash

# These codes assemble the trimmed short reads 
# Sprindzuk Matvey, software engineer, bioinformatics_bel@yahoo.com


./prokka/bin/prokka pilon_output.pilon.fasta -- outdir prokkacurrentassembly_pilon --prefix prokka_assembled_2019 --force

#./prokka/bin/prokka genome_assembled_with_a5_miseq.fasta -- outdir prokkacurrentassembly_denovo_a5 --prefix prokka_a5_assembled_2019 --force

# ./prokka/bin/prokka /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/ENGINE/genome_assembled_with_a5_miseq.s2/genome_assembled_with_a5_miseq.ec.fasta -- outdir prokkacurrentassembly_denovo_a5 --prefix prokka_a5_assembled_2019 --force


mv *PROKKA* /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder

# mv prokkacurrentassembly_denovo_a5 /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder




#mv *.fastq  /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder # moving processed files into the one single folder
#mv *.fasta /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.gz /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.csv /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.tmplibs  /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.bai /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.qvl  /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.bam  /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.agp  /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv *.fsa  /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv genome_assembled_with_a5_miseq.s1 /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder
#mv genome_assembled_with_a5_miseq.s2 /home/mat29/Desktop/TuberculosisGenomicsProcessor/MAIN/temporary_folder

# the end
 
# thank you for your patience and attention


















