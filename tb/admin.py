from django.contrib import admin

# Register your models here.
 
# Register your models here.
from .models import GenomeDataProcessingRequest
 
class NoteAdmin(admin.ModelAdmin):
    class Meta:
        model = GenomeDataProcessingRequest
 
admin.site.register(GenomeDataProcessingRequest,NoteAdmin)
admin.site.site_header = 'Tuberculosis data processing service'